﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgregatorLinkow.Models
{
    public class Link
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(40)")]
        [MaxLength(40)]
        public string Name { get; set; }
        [Column(TypeName = "varchar(250)")]
        [MaxLength(250)]
        public string Description {get; set;}
        [Required]
        [Column(TypeName = "varchar(250)")]
        [MaxLength(250)]
        public string Url { get; set; }
        public DateTime DateAdded { get; set; } //Data dodania linku
        [Column(TypeName = "varchar(250)")]
        public string UserId { get; set; } //Id użytkownika, przez którego został dodany link
        [Required]
        public int Likes { get; set; } //Liczba polubień
    }
}
