﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AgregatorLinkow.Models
{
    public class Like
    {
        [Key]
        public int LikeId { get; set; }
        [Required]
        [Column(TypeName = "varchar(250)")]
        public string UserId { get; set; }
        [Required]
        public int LinkId { get; set; }
    }
}
