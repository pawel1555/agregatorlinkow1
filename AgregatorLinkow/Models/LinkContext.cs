﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AgregatorLinkow.Models
{
    public class LinkContext : IdentityDbContext<IdentityUser>
    {
        public LinkContext(DbContextOptions<LinkContext> options) : base(options)
        {

        }

        public DbSet<Link> Links { get; set; }
        public DbSet<Like> Likes { get; set; }
    }
}
