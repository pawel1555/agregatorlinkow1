﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AgregatorLinkow.Models;
using Microsoft.AspNetCore.Identity;
using System.Diagnostics;

namespace AgregatorLinkow.Controllers
{
    public class LinksController : Controller
    {
        private readonly LinkContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public LinksController(LinkContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber) //Strona główna
        {
            var links = from l in _context.Links select l;
            links = links.Where(l => DateTime.Compare(l.DateAdded, DateTime.Now.AddDays(-5)) >= 0); //Warunek dot. daty - wyświetlania max. 5 dni od dodania
            links = links.OrderByDescending(l => l.Likes); //Sortowanie według ilości polubień

            return View(await PaginatedList<Link>.CreateAsync(links.AsNoTracking(), pageNumber ?? 1, 100));
        }

        [HttpGet]
        public async Task<IActionResult> UserLinks(int? pageNumber) //Wszystkie linki dodane przez zalogowanego użytkownika
        {
            var loggedUserId = _userManager.GetUserId(HttpContext.User);
            var links = from l in _context.Links select l;
            links = links.Where(l => l.UserId == loggedUserId);
            links = links.OrderByDescending(l => l.Likes);

            return View(await PaginatedList<Link>.CreateAsync(links.AsNoTracking(), pageNumber ?? 1, 100));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new Link());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Url")] Link link)
        {
            ViewBag.Message = ModelState.IsValid;
            if (ModelState.IsValid)
            {
                link.DateAdded = DateTime.Now;
                link.UserId = _userManager.GetUserId(HttpContext.User);
                link.Likes = 0;
                _context.Links.Add(link);
                await _context.SaveChangesAsync();
            }
            return View(link);
        }

        [HttpPost]
        public async Task<JsonResult> Like(int id)
        {
            var link = await _context.Links.FindAsync(id);
            var loggedUserId = _userManager.GetUserId(HttpContext.User);

            if (link != null)
            {
                link.Likes++;
                _context.Links.Update(link);
                AddNewLike(loggedUserId, id);

                await _context.SaveChangesAsync();
                return Json(true);
            }
            return Json(false);
        }

        private void AddNewLike(string userId, int linkId)
        {
            Like like = new Like();
            like.UserId = userId;
            like.LinkId = linkId;
            _context.Likes.Add(like);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
